package Alkeze.ergoApi.Domain;


import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.LocalDateTime;

@Entity
@Table
@ToString(of = {"NationalID", "FirstName","LsatName","Gender","BirthDay","Nationality"})
@EqualsAndHashCode(of = {"id", "NationalID"})
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer NationalID;
    private String FirstName;
    private String LsatName;
    private String Gender;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDateTime BirthDay;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNationalID() {
        return NationalID;
    }

    public void setNationalID(Integer nationalID) {
        NationalID = nationalID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLsatName() {
        return LsatName;
    }

    public void setLsatName(String lsatName) {
        LsatName = lsatName;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public LocalDateTime getBirthDay() {
        return BirthDay;
    }

    public void setBirthDay(LocalDateTime birthDay) {
        BirthDay = birthDay;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String nationality) {
        Nationality = nationality;
    }

    private String Nationality;

}

