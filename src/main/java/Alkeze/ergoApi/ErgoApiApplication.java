package Alkeze.ergoApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ErgoApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ErgoApiApplication.class, args);
	}

}
