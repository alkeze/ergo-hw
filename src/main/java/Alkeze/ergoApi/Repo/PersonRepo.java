package Alkeze.ergoApi.Repo;

import Alkeze.ergoApi.Domain.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepo extends JpaRepository<Person, Long>{

}