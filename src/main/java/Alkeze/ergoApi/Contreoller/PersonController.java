package Alkeze.ergoApi.Contreoller;

import Alkeze.ergoApi.Domain.Person;
import Alkeze.ergoApi.Repo.PersonRepo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("person")
public class PersonController {
    private final PersonRepo personRepo;

    @Autowired
    public PersonController(PersonRepo personRepo) {
        this.personRepo = personRepo;
    }


    @GetMapping
    public List<Person> list() {
        return personRepo.findAll();
    }

    @GetMapping("{NationalID}")
    public Person getOne(@PathVariable("NationalID") Person person) {
        return person;
    }


    @PostMapping
    public Person create(@RequestBody Person person) {
        return personRepo.save(person);
    }

    @PutMapping("{NationalID}")
    public Person update(@PathVariable("NationalID") Person personFromDb,
                         @RequestBody Person person) {
        BeanUtils.copyProperties(person, personFromDb, "NationalID");
        return personRepo.save(personFromDb);
    }

    @DeleteMapping("{NationalID}")
    public void delete(@PathVariable("NationalID") Person person) {
        personRepo.delete(person);
    }
}
